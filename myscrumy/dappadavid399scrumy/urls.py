from django.urls import path
from . import views


app_name = 'dappadavid399scrumy'

urlpatterns = [
    path('',views.index),
    path('getmark/', views.get_grading_parameters),
]
